### 火车票查询

```
只能用于车票查询....
```

输出：

`success`

```python
$ python train.py
出发站和到达站需要输入中文名称，日期输入格式为YYYY-MM-DD，切记！！！
出发站：北京
到达站：上海
输入日期：2015-12-25
######### 北京南--上海虹桥 #########
车型：高速动车||车次：G139
出发时间：14:10||到达时间：19:51||历时：5小时41分
座位：二等座||票价：553||余票：794
座位：一等座||票价：933||余票：108
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G117
出发时间：09:43||到达时间：15:41||历时：5小时58分
座位：二等座||票价：553||余票：936
座位：一等座||票价：933||余票：47
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G119
出发时间：10:05||到达时间：15:45||历时：5小时40分
座位：二等座||票价：553||余票：398
座位：一等座||票价：933||余票：14
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G15
出发时间：11:00||到达时间：15:55||历时：4小时55分
座位：二等座||票价：553||余票：562
座位：一等座||票价：933||余票：81
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G3
出发时间：14:00||到达时间：18:48||历时：4小时48分
座位：二等座||票价：553||余票：437
座位：一等座||票价：933||余票：105
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G13
出发时间：10:00||到达时间：14:55||历时：4小时55分
座位：二等座||票价：553||余票：550
座位：一等座||票价：933||余票：109
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G111
出发时间：08:35||到达时间：14:21||历时：5小时46分
座位：二等座||票价：553||余票：950
座位：一等座||票价：933||余票：18
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G19
出发时间：16:00||到达时间：21:16||历时：5小时16分
座位：二等座||票价：553||余票：684
座位：一等座||票价：933||余票：133
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G113
出发时间：09:05||到达时间：14:29||历时：5小时24分
座位：二等座||票价：553||余票：672
座位：一等座||票价：933||余票：145
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G17
出发时间：15:00||到达时间：19:56||历时：4小时56分
座位：二等座||票价：553||余票：603
座位：一等座||票价：933||余票：127
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G1
出发时间：09:00||到达时间：13:48||历时：4小时48分
座位：二等座||票价：553||余票：673
座位：一等座||票价：933||余票：76
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G115
出发时间：09:32||到达时间：15:14||历时：5小时42分
座位：二等座||票价：553||余票：663
座位：一等座||票价：933||余票：118
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G153
出发时间：17:15||到达时间：22:51||历时：5小时36分
座位：二等座||票价：553||余票：785
座位：一等座||票价：933||余票：108
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G135
出发时间：13:02||到达时间：18:59||历时：5小时57分
座位：二等座||票价：553||余票：670
座位：一等座||票价：933||余票：171
######### Good Lucky #########

######### 北京南--上海 #########
车型：动车组||车次：D311
出发时间：21:16||到达时间：08:58||历时：11小时42分
座位：二等座||票价：309||余票：51
座位：软卧上||票价：615||余票：383
座位：软卧下||票价：696||余票：383
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G137
出发时间：13:45||到达时间：19:26||历时：5小时41分
座位：二等座||票价：553||余票：780
座位：一等座||票价：933||余票：89
######### Good Lucky #########

######### 北京--上海 #########
车型：其他||车次：1461
出发时间：11:54||到达时间：08:11||历时：20小时17分
座位：硬座||票价：156.5||余票：1340
座位：硬卧上||票价：283.5||余票：23
座位：硬卧中||票价：294.5||余票：23
座位：硬卧下||票价：304.5||余票：23
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G157
出发时间：17:43||到达时间：23:23||历时：5小时40分
座位：二等座||票价：553||余票：737
座位：一等座||票价：933||余票：83
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G11
出发时间：08:00||到达时间：13:09||历时：5小时9分
座位：二等座||票价：553||余票：654
座位：一等座||票价：933||余票：118
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G131
出发时间：12:25||到达时间：18:14||历时：5小时49分
座位：二等座||票价：553||余票：414
座位：一等座||票价：933||余票：12
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G133
出发时间：12:52||到达时间：18:36||历时：5小时44分
座位：二等座||票价：553||余票：709
座位：一等座||票价：933||余票：77
######### Good Lucky #########

######### 北京--上海 #########
车型：空调特快||车次：T109
出发时间：19:33||到达时间：10:44||历时：15小时11分
座位：硬座||票价：177.5||余票：91
座位：硬卧上||票价：304.5||余票：140
座位：硬卧中||票价：315.5||余票：140
座位：硬卧下||票价：325.5||余票：140
座位：软卧上||票价：476.5||余票：34
座位：软卧下||票价：497.5||余票：34
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G43
出发时间：14:05||到达时间：19:38||历时：5小时33分
座位：二等座||票价：553||余票：482
座位：一等座||票价：933||余票：110
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G41
出发时间：09:17||到达时间：14:46||历时：5小时29分
座位：二等座||票价：553||余票：545
座位：一等座||票价：933||余票：75
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G129
出发时间：12:15||到达时间：17:49||历时：5小时34分
座位：二等座||票价：553||余票：709
座位：一等座||票价：933||余票：80
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G109
出发时间：08:15||到达时间：14:00||历时：5小时45分
座位：二等座||票价：553||余票：793
座位：一等座||票价：933||余票：106
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G107
出发时间：08:05||到达时间：13:26||历时：5小时21分
座位：二等座||票价：553||余票：758
座位：一等座||票价：933||余票：104
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G101
出发时间：07:00||到达时间：12:37||历时：5小时37分
座位：二等座||票价：553||余票：927
座位：一等座||票价：933||余票：39
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G105
出发时间：07:36||到达时间：13:21||历时：5小时45分
座位：二等座||票价：553||余票：433
座位：一等座||票价：933||余票：25
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G141
出发时间：14:31||到达时间：20:13||历时：5小时42分
座位：二等座||票价：553||余票：806
座位：一等座||票价：933||余票：84
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G103
出发时间：07:05||到达时间：12:42||历时：5小时37分
座位：二等座||票价：553||余票：437
座位：一等座||票价：933||余票：26
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G145
出发时间：15:15||到达时间：21:07||历时：5小时52分
座位：二等座||票价：553||余票：697
座位：一等座||票价：933||余票：83
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G127
出发时间：11:36||到达时间：17:16||历时：5小时40分
座位：二等座||票价：553||余票：388
座位：一等座||票价：933||余票：20
######### Good Lucky #########

######### 北京南--上海 #########
车型：动车组||车次：D321
出发时间：21:23||到达时间：09:12||历时：11小时49分
座位：二等座||票价：309||余票：86
座位：软卧上||票价：615||余票：448
座位：软卧下||票价：696||余票：448
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G21
出发时间：17:00||到达时间：22:39||历时：5小时39分
座位：二等座||票价：553||余票：662
座位：一等座||票价：933||余票：138
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G143
出发时间：14:36||到达时间：20:33||历时：5小时57分
座位：二等座||票价：553||余票：800
座位：一等座||票价：933||余票：91
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G125
出发时间：11:10||到达时间：16:59||历时：5小时49分
座位：二等座||票价：553||余票：919
座位：一等座||票价：933||余票：13
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G149
出发时间：16:25||到达时间：22:24||历时：5小时59分
座位：二等座||票价：553||余票：824
座位：一等座||票价：933||余票：92
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G123
出发时间：11:05||到达时间：16:42||历时：5小时37分
座位：二等座||票价：553||余票：416
座位：一等座||票价：933||余票：0
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G411
出发时间：11:20||到达时间：17:07||历时：5小时47分
座位：二等座||票价：553||余票：405
座位：一等座||票价：933||余票：0
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G147
出发时间：16:05||到达时间：21:54||历时：5小时49分
座位：二等座||票价：553||余票：842
座位：一等座||票价：933||余票：105
######### Good Lucky #########

######### 北京南--上海虹桥 #########
车型：高速动车||车次：G121
出发时间：10:28||到达时间：16:27||历时：5小时59分
座位：二等座||票价：553||余票：912
座位：一等座||票价：933||余票：12
######### Good Lucky #########
```

`fail`

```python
$ python train.py
出发站和到达站需要输入中文名称，日期输入格式为YYYY-MM-DD，切记！！！
出发站：123
到达站：123
输入日期：123
请重新输入!
```

#/usr/bin/env python
#coding:utf-8

import urllib2,json

print  "出发站和到达站需要输入中文名称，日期输入格式为YYYY-MM-DD，切记！！！"
def Train():
    from_z = raw_input("出发站：")
    to = raw_input("到达站：")
    date_z = raw_input("输入日期：")
    url = "http://apis.baidu.com/qunar/qunar_train_service/s2ssearch?version=1.0&from=" + from_z + "&to=" + to + "&date=" + date_z
    req = urllib2.Request(url)
    req.add_header("apikey","XXXXXXXXXXXXXX")
    content = urllib2.urlopen(req).read()
    Json_content = json.JSONDecoder().decode(content)
    for i in Json_content['data']['trainList']:
        #print u'出发站：' + i['from'] + "||" + u'到达站：' + i['to']
        print "######### " + i['from'] + "--" + i['to'] + " #########"
        print u'车型：' + i['trainType'] + "||" + u'车次：' + i['trainNo']
        print u'出发时间：' + i['startTime'] + "||" + u'到达时间：' + i['endTime'] + "||" + u'历时：' + i['duration']
        #print u'座位：' + i['seatInfos'][0]['seat'] + "||" + u'票价：' + i['seatInfos'][0]['seatPrice']
        #print "######### " + u'余票：' + i['seatInfos'][0]['remainNum'] + " #########"
        for j in i['seatInfos']:
            seatinfo = u'座位：' + j['seat'] + "||" + u'票价：' + j['seatPrice']
            if j['remainNum'] == -1:
                print seatinfo + "||" +'余票情况未知'
            else:
                print seatinfo + "||" +u'余票：' + str(j['remainNum'])
        print "######### Good Lucky #########\n"
    #print Json_content

try:
    Train()
except:
    print u'请重新输入!'
